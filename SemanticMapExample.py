from SemanticMap import SemanticMap
import os

mapsFolder = './maps'
#mapName = 'Falcon'
#mapName = 'Falcon_Easy'
#mapName = 'Falcon_Medium'
mapName = 'Falcon_Hard'

print ("Loading SemanticMap from: " + os.path.join(mapsFolder, mapName + '_sm.json'))
sm = SemanticMap()
sm.load_semantic_map(os.path.join(mapsFolder, mapName + '_sm.json'))

# close to DP156 in RH
sx = -2075
sz = 179

# close to DP38 in CHM
ex = -2054
ez = 166

print ("Computing the Shortest path...")
dist, sdist, edist, pathIds = sm.get_shortest_path (sx, sz, ex, ez)

print(' - Distance from starting Decision Point to Ending Decision Point: ' + str(dist) + ' blocks')
print(' - Distance from (x1, z1) to starting Decision Point: ' + str(sdist) + ' blocks')
print(' - Distance from (x2, z2) to ending Decision Point: ' + str(edist) + ' blocks')
print(' - Decision Point path to take: ' + str(pathIds))
for dp_id in pathIds:
    dp = sm.get_decision_point(dp_id, use_updated_map=False)
    print('   ' + dp_id + ' in ' + dp['area_id'] + (' : ' + dp['sub_area_id'] if 'sub_area_id' in dp.keys() else ''))
