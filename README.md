# ASIST SemanticMap Class Utility Library

SemanticMap.py is a utility class for handing ASIST Semantic Maps.  I also has code to handle IHMC's extension with adds a Decision Graph with Decision Points and Paths between the Decision Points

## Semantic Maps

ASIST had defined the semantic map representation as a list of locations, connections, and objects.  You can read the definition here if you have access to the ASIST Google Doc: https://docs.google.com/document/d/1HfVlOgaxqAgMjvYN2kTsfYqz9xpmfyq3DQRVbb9gqbQ/edit#heading=h.l9ajcjaf5d92

## IHMC Decision Graph Extension

IHMC runs the ASIST testbed with code for Monte Carlo simulations and simulated human players.  To facililitate running the simulation from code we use decision graphs.  We extended the Semantic Map to include `decision_points` and `paths` between them.  

Here is an image of a decision graph drawn on top of the Falcon Hard semantic map:

![Falcon Hard Decision Graph Image](Falcon_Hard_Decision_graph.png)

The blue circles are decision points and the blue lines between them are the paths.  If a line is red then that path's visibility is blocked or it is blocked by obstacles.

The addition to the Semantic Map is as follows:

    "extensions": {
        "ihmc_simasist": {
            "decision_points": [...],
            "paths": [...]
        }
    }

### decision_points

Decision points are locations where the subject needs to decide what to do next.  Do they follow path 1 or path 2, do they triage a victim, do they clear debris or put out a fire.  Decision points are formated as follows:

    {
      "id": "dp23",
      "area_id": "es1",
      "sub_area_id": "es1_2",
      "bounds": {
        "type": "point",
        "coordinates": [
          {
            "x": -2074.5,
            "z": 150.5
          }
        ]
      },
      "victims": {
        "critical": 0,
        "non_critical": 1
      }
    }

- "id" - is the unique id of the decision point.
- "area_id" - is the id of the Semantic Map Location the decision point is in.
- "sub_area_id" - (optional) is the id of the Semantic Map Child Location the decision point is in.
- "bounds" - contains the Semantic Map formatted point corrdinates of the decision point.
- "victims" - (optional) contains the number and type of victims at this decision point.

### paths

Paths contain information about how to get from one decision point to another and include any obstacles which may be blocking the path.  Path objects are formatted as follows:

    {
      "id": "p-dp178-dp123",
      "distance": 2,
      "loc_id_1": "dp178",
      "loc_id_2": "dp123",
      "connection_id": "c_135_89_137_90",
      "visibility_blocked": true,
      "obstacles": [
        {
            "type": "debris",
            "amount": 2
        }
      ]
    },

- "id" - the unique id of the path.
- "distance" - The distance in block it takes to follow this path.
- "loc_id_1" - The Decision point this path starts at.
- "loc_id_2" - The Decision point this path ends at.
- "connection_id": (optional) The Semantic Map connection this path is associated with.
- "visibility_blocked": (optional) If true the subject cannot see between the decision points.  This can be caused by debris, a door, or something else like a wall or desk.
- "obstacles" - (optional) This is a list of type and amount of obstacles which may be blocking the subject from traversing this path.

### SemanticMap class

The SemanticMap class provides methods for loading and using semantic maps which are in JSON.  One example of how to use the class is to get the distance and path between two points.  The example in SemanticMapExample.py shows how to do this by using the `load_semantic_map` and `get_shortest_path` methods.